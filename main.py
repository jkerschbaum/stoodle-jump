import pygame
from pygame.locals import *

from globals import WIDTH, HEIGHT

from main_menu import main_menu

# Esc zum beenden
vec = pygame.math.Vector2

# Globals:

IS_FULLSCREEN = False

if IS_FULLSCREEN:
    displaysurface = pygame.display.set_mode((WIDTH, HEIGHT), FULLSCREEN)
else:
    displaysurface = pygame.display.set_mode((WIDTH, HEIGHT))
pygame.display.set_caption("Stoodle Jump")


while True:
    main_menu(displaysurface)
