# Stoodle Jump

(Working Title)

Experiment comparing the effectiveness of various models in generalising on complex tasks.
Examples are collected through human play of the game and then used to train the controllers that are implemented through a neural net.
Reinforcement learning controllers (currently only one, running DQN) do not use the preconstructed examples.

Entry point is main.py
