import pygame
from pygame.locals import QUIT, K_LEFT, K_RIGHT, K_UP
import random
import sys
import time
from math import floor

from globals import WIDTH, HEIGHT, FPS, platforms, all_sprites

pygame.init()
score_font = pygame.font.SysFont("Verdana", 20)
TIMER = 20
FramePerSec = pygame.time.Clock()
CURRENT_MAX_SCORE = 1

player = None

def get_player():
    """
    Returns player sprite object
    :return: Player sprite
    """
    return player


class Platform(pygame.sprite.Sprite):
    def __init__(self, max_speed, may_move):
        super().__init__()
        self.surf = pygame.Surface((random.randint(50, 100), 12))
        self.surf.fill((255, 0, 0))
        self.rect = self.surf.get_rect(center=(random.randint(0, WIDTH-10), random.randint(0, HEIGHT-30)))
        self.point = 0
        self.speed = 0.0
        if may_move:
            self.speed = random.uniform(0 - max_speed, max_speed)
            if random.randint(0, 2) == 0:
                self.speed = 0.0
        else:
            self.speed = 0.0
        self.moving = (self.speed != 0.0)

    def set_score(self):
        """
        Sets score of this platform
        :return: None
        """
        global CURRENT_MAX_SCORE
        self.point = CURRENT_MAX_SCORE
        CURRENT_MAX_SCORE += 1

    def move(self):
        """
        If the Platform is moving, move it
        :return: None
        """
        if self.moving:
            self.rect.move_ip(self.speed, 0)
            if self.speed > 0 and self.rect.left > WIDTH:
                self.rect.right = 0
            if self.speed < 0 and self.rect.right < 0:
                self.rect.left = WIDTH


def generate_next_platform(may_move):
    """
    Generates new platform if fewer than 7 are on screen
    :param may_move: True if platform to be generated may have spped != 0
    :return: None
    """
    while len(platforms) < 7:
        width = random.randrange(50, 100)
        while True:
            p = Platform(2, may_move)
            p.rect.center = (random.randrange(0, WIDTH - width), random.randrange(-50, 0))
            if not too_close(p, platforms):
                break
        p.set_score()
        platforms.add(p)
        all_sprites.add(p)


def scroll_screen(player_sprite):
    """
    Scrolls screen if player is in top third
    :param player_sprite: Sprite representing the player
    :return: None
    """
    if player_sprite.rect.top <= HEIGHT / 3:
        player_sprite.pos.y += abs(player_sprite.vel.y)
        for plat in platforms:
            plat.rect.y += abs(player_sprite.vel.y)
            if plat.rect.top >= HEIGHT:
                plat.kill()


def too_close(platform, groupies):
    """
    Checks if platform is too close to any other. Has 1% chance to allow anyway, in case of no legal position
    :param platform: Platform to be checked
    :param groupies: Set of platforms to be checked against
    :return: True or False
    """
    min_dist = 50
    if random.randint(0, 99) == 1:
        # Fixes issue where the game sometimes cannot generate plattforms far enough apart
        # Yes, this should be a more sophisticated check, but this is easier
        return False
    if pygame.sprite.spritecollideany(platform, groupies):
        return True
    else:
        for plat in groupies:
            if plat == platform:
                continue
            if (abs(platform.rect.top - plat.rect.bottom) < min_dist) or\
                    (abs(platform.rect.bottom - plat.rect.top) < min_dist):
                return True
        return False


def handle_events():
    """
    Handles various events that may occur during gameloop. Currently handles ESC -> quit
    :return: None
    """
    for event in pygame.event.get():
        if event.type == QUIT or (event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE):
            pygame.quit()
            sys.exit()


def redraw_screen(player_sprite, current_timer, do_timer, displaysurface):
    """
    Redraws screen according to current sprites, timer and score
    :param player_sprite: Sprite representing the player
    :param current_timer: Current Timer value
    :param do_timer: Should the timer be shown?
    :param displaysurface: Display to draw to
    :return: None
    """
    displaysurface.fill((0, 0, 0))
    score_text = score_font.render(str(player_sprite.score), True, (123, 255, 0))
    displaysurface.blit(score_text, (WIDTH / 2, 10))
    pressed_keys = player_sprite.last_presses
    key_names = ["Left", "Right", "Jump"]
    for i in range(len(pressed_keys)):
        if pressed_keys[i]:
            key_text = score_font.render(key_names[i], True, (255, 255, 0))
        else:
            key_text = score_font.render(key_names[i], True, (255, 0, 0))
        displaysurface.blit(key_text, (WIDTH / 2, (10 + ((i+1) * 20))))
    displaysurface.blit(player_sprite.surf, player_sprite.rect)
    if do_timer:
        timer_font = pygame.font.SysFont("couriernew", 20)
        timer_text = timer_font.render(str(current_timer), True, (123, 255, 0))
        displaysurface.blit(timer_text, (WIDTH - timer_text.get_rect().width - 10, 30))
    for entity in platforms:
        entity.move()
        displaysurface.blit(entity.surf, entity.rect)
    pygame.display.update()


def check_game_over(player_sprite, current_timer, do_time, displaysurface):
    """
    Checks if the player has entered a game-over state, ie if the player is below the screen or the timer has run out
    :param player_sprite: Sprite representing the player
    :param current_timer: Current value of the timer
    :param do_time: Should the timer be checked?
    :param displaysurface: Display to draw to
    :return: True or False
    """
    global player
    if (do_time and current_timer <= 0) or (player_sprite.rect.top > HEIGHT):
        player = None
        for entity in all_sprites:
            entity.kill()
            all_sprites.remove(entity)
            displaysurface.fill((0, 0, 0))
            for entity2 in all_sprites:
                displaysurface.blit(entity2.surf, entity2.rect)
            score_text = score_font.render(str(player_sprite.score), True, (123, 255, 0))
            displaysurface.blit(score_text, (WIDTH / 2, 10))
            pygame.display.update()
            time.sleep(0.2)
        # Reenable to close game on loss
        # pygame.quit()
        # sys.exit()
        return True
    else:
        return False


def get_current_state():
    """
    Returns current state of the screen
    :return: Tuple, first element a list of 2D distances of platforms to player, second element list of keys pressed
    """
    positions = []
    for sprite in platforms.sprites():
        positions.append(sprite.rect.center)
    player_position = all_sprites.sprites()[0].pos
    player_position = (player_position.x, player_position.y)
    for i in range(len(positions)):
        positions[i] = [positions[i][0] - player_position[0], positions[i][1] - player_position[1]]
    # Normalising distances for screen wrap
    for pos in positions:
        if pos[0] > (WIDTH / 2):
            pos[0] = pos[0] - WIDTH
        elif pos[0] < (0 - (WIDTH/2)):
            pos[0] = pos[0] + WIDTH
    pressed = pygame.key.get_pressed()
    return positions, (pressed[K_LEFT], pressed[K_RIGHT], pressed[K_UP])

def write_example_to_file(current_state, keys, timer):
    """
    Writes new example to file
    :param current_state: Current state of the board
    :param keys: List of bools representing inputs dones
    :param timer: Current time remaining
    :return: None
    """
    line = ""
    for plat in current_state:
        line += (str(plat[0]) + " ~ " + str(plat[1]) + " | ")
    line += "#"
    for k in keys:
        line += " " + str(k)
    line += " # "
    line += str(timer)
    line += "\n"
    with open("examples.dat", "a", encoding="utf-8") as file:
        file.writelines([line])


def init_level(moving, controller_constructor):
    """
    Initialises level
    :param moving: Should moving platforms be generated?
    :param controller_constructor: Constructor for the controller to be used for this run
    :return: None
    """
    global all_sprites, platforms, CURRENT_MAX_SCORE, player
    all_sprites.empty()
    platforms.empty()
    CURRENT_MAX_SCORE = 0
    player_sprite = controller_constructor()
    player = player_sprite
    all_sprites.add(player_sprite)
    ground = Platform(2, moving)
    ground.surf = pygame.Surface((WIDTH, 20))
    ground.surf.fill((255, 0, 0))
    ground.rect = ground.surf.get_rect(center=(WIDTH / 2, HEIGHT - 10))
    ground.speed = 0
    ground.moving = False
    all_sprites.add(ground)
    platforms.add(ground)
    for x in range(6):
        p = Platform(2, moving)
        while True:
            if not too_close(p, platforms):
                break
            else:
                p = Platform(2, moving)
        p.set_score()
        platforms.add(p)
        all_sprites.add(p)


def gameloop(player_sprite, moving, do_timer, do_write_examples, displaysurface):
    """
    Main game loop
    :param player_sprite: Sprite representing the player
    :param moving: Should moving platforms be generated?
    :param do_timer: Should a timer be used?
    :param do_write_examples: Should examples be written?
    :param displaysurface: Display to draw to
    :return: Players score
    """
    start_time = pygame.time.get_ticks()
    target_time = start_time + (TIMER * 1000)
    while True:
        current_timer = floor((target_time - pygame.time.get_ticks()) / 1000)
        player_sprite.move(platforms, current_timer, player_sprite.pos)
        handle_events()
        if do_write_examples:
            current_state, keys = get_current_state()
            if do_timer:
                cur_timer = current_timer
            else:
                cur_timer = -1
            # Many frames have no key presses (approx. 50%), so we save only 1/2 of those, since they are less relevant
            # We also save just 1/4 of all examples after that, to minimize the influence of outliers
            if ((not keys == [False, False, False]) or random.randint(0, 1) == 0) and random.randint(0, 3) == 0:
                write_example_to_file(current_state, keys, cur_timer)
        scroll_screen(player_sprite)
        generate_next_platform(moving)
        redraw_screen(player_sprite, current_timer, do_timer, displaysurface)
        FramePerSec.tick(FPS)
        has_lost = check_game_over(player_sprite, current_timer, do_timer, displaysurface)
        if has_lost:
            break
        if player_sprite.supports_online_learning:
            # TODO: Actual online learning logic here
            continue
    return player_sprite.score

def game(moving, do_timer, controller_constructor, displaysurface, do_write_examples=False):
    """
    Convininece function for game initialisation and loop
    :param moving: Should moving platforms be generated?
    :param do_timer: Should a timer be used?
    :param controller_constructor: Constructor-function for desired controller object
    :param displaysurface: Display to draw to
    :param do_write_examples: Should examples be written to file?
    :return: Players score
    """
    init_level(moving, controller_constructor)
    return gameloop(all_sprites.sprites()[0], moving, do_timer, do_write_examples, displaysurface)