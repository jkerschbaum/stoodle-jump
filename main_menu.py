import pygame
import sys

from globals import WIDTH, HEIGHT
from game import game
from controllers import Player, Random, FFNN, Plan, Functional, DQN, Complex, Classes

controllers = [(Player, "Human"), (Random, "Random"), (Plan, "Planned"),  (FFNN, "Feed-Forward NN"),
               (Functional, "Functional API 1"), (Complex, "Functional API 2"), (Classes, "Functional API 3")]

reinforcement_controllers = [(DQN, "Deep Q Network")]


def main_menu(displaysurface):
    """
    Main Menu
    :param displaysurface: Display to draw to
    :return: None
    """
    selection_made = False
    current_selection = 0
    options = ["Controller, static", "Controller, moving", "Reinforcement Learner (DQN)",
               "Re-learn batch controllers", "Quit"]
    options_texts = []
    options_recs = []
    secondary_level = 0
    controller_selection = 0
    while not selection_made:
        for event in pygame.event.get():
            if event.type == pygame.QUIT or (event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE):
                pygame.quit()
                sys.exit()
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_UP:
                    if current_selection > 0:
                        current_selection -= 1
                elif event.key == pygame.K_DOWN:
                    if current_selection < len(options) - 1:
                        current_selection += 1
                elif event.key == pygame.K_LEFT:
                    if secondary_level > 0:
                        secondary_level -= 1
                elif event.key == pygame.K_RIGHT:
                    if secondary_level < 1:
                        secondary_level += 1
                elif event.key == pygame.K_PLUS:
                    if controller_selection < len(controllers)-1:
                        controller_selection += 1
                elif event.key == pygame.K_MINUS:
                    if controller_selection > 0:
                        controller_selection -= 1
                if event.key == pygame.K_RETURN:
                    selection_made = True
        displaysurface.fill((255, 255, 255))
        if secondary_level == 0:
            watch_image = pygame.image.load("./resources/watch_off.png")
        else:
            watch_image = pygame.image.load("./resources/watch_on.png")
        watch_image = pygame.transform.scale(watch_image, (30, 39))
        displaysurface.blit(watch_image, (WIDTH - 60, (HEIGHT / 2) - 20))
        title_font = pygame.font.SysFont("couriernew", 40)
        title = title_font.render("Stoodle Jump", 40, (0, 0, 0))
        controller_font = pygame.font.SysFont("couriernew", 20)
        controller_text = controller_font.render(controllers[controller_selection][1], 20, (0, 0, 0))
        displaysurface.blit(controller_text, (20, 215))
        title_rect = title.get_rect()
        options_recs.clear()
        options_texts.clear()
        for option in options:
            if current_selection == options.index(option):
                option_font = pygame.font.SysFont("couriernew", 15)
                option_text = option_font.render(option, 40, (255, 0, 0))
                options_texts.append(option_text)
                options_recs.append(option_text.get_rect())
            else:
                option_font = pygame.font.SysFont("couriernew", 15)
                option_text = option_font.render(option, 40, (0, 0, 0))
                options_texts.append(option_text)
                options_recs.append(option_text.get_rect())

        # Main Menu Text
        displaysurface.blit(title, (WIDTH / 2 - (title_rect[2] / 2), 80))
        for i in range(len(options)):
            displaysurface.blit(options_texts[i], (WIDTH / 2 - (options_recs[i][2] / 2), 300 + (i * 20)))
        pygame.display.update()
    if current_selection == len(options) - 1:
        pygame.quit()
        sys.exit()
    if current_selection == 0:
        game(False, secondary_level == 1, controllers[controller_selection][0], displaysurface, do_write_examples=(controller_selection==0))
    if current_selection == 1:
        game(True, secondary_level == 1, controllers[controller_selection][0], displaysurface)
    if current_selection == 2:
        # Reinforcement Learners
        cont = reinforcement_controllers[0][0]()
        cont.reinforcement_learning(10, displaysurface)
        game(False, True, cont.constructor_dummy, displaysurface)
    if current_selection == 3:
        # TODO: Learn Batch Controls
        FFNN().batch_learn()
        Functional().batch_learn()
        Complex().batch_learn()
        Classes().batch_learn()
        # Note: capture examples from human player static mode