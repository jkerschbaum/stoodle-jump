from controllers.base_controller import BaseController
from globals import WIDTH
from game import get_current_state
import math


class Plan(BaseController):

    def __init__(self):
        super().__init__()

    def get_keys_to_press(self, platforms, current_timer, player_pos):
        state = get_current_state()
        state = state[0]
        distances = []
        for i in range(len(state)):
            distances.append(math.sqrt((state[i][0] ** 2) + (state[i][1] ** 2 * 6)))
        min_distance_index = distances.index(min(distances))
        keys_to_press = []
        if state[min_distance_index][0] > 0:
            keys_to_press.append(False)
            keys_to_press.append(True)
        else:
            keys_to_press.append(True)
            keys_to_press.append(False)
        return keys_to_press + [True]
