from controllers.random_controller import Random
from controllers.human_player import Player
from controllers.ffnn_controller import FFNN
from controllers.planned_controller import Plan
from controllers.DQN_controller import DQN
from controllers.functional_controller import Functional
from controllers.complex_controller import Complex
from controllers.classes_controller import Classes