from controllers.base_controller import BaseController
import tensorflow as tf
from globals import WIDTH, plot_custom_model
from collections import Counter


class FFNN(BaseController):
    def __init__(self):
        super().__init__()
        print("Init")
        self.supports_batch_learning = True
        self.model = self.create_model()
        self.threshold = 0.3
        self.model.compile(optimizer=tf.keras.optimizers.Adam(0.001), loss=tf.keras.losses.MeanSquaredError())
        try:
            # Load model
            self.model.load_weights(".\\controllers\\models\\ffnn.ckpt")
        except:
            # Build new model
            self.batch_learn()
        plot_custom_model(self.model, "ffnn")

    def create_model(self):
        """
        Creates model for FFNN Controller
        :return: Model
        """
        model = tf.keras.Sequential([
            tf.keras.layers.Dense(15, activation='sigmoid', input_shape=(15,)),
            tf.keras.layers.Dense(5, activation='sigmoid'),
            tf.keras.layers.Dense(3, activation='sigmoid')
        ])
        model.summary()
        return model

    def get_keys_to_press(self, platforms, current_timer, player_pos):
        positions = []
        for sprite in platforms.sprites():
            positions.append(sprite.rect.center)
        player_pos = (player_pos.x, player_pos.y)
        for i in range(len(positions)):
            positions[i] = [positions[i][0] - player_pos[0], positions[i][1] - player_pos[1]]
        # Normalising distances for screen wrap
        for pos in positions:
            if pos[0] > (WIDTH / 2):
                pos[0] = pos[0] - WIDTH
            elif pos[0] < (0 - (WIDTH / 2)):
                pos[0] = pos[0] + WIDTH
        positions = [val for sublist in positions for val in sublist]
        inputs = positions + [current_timer]
        result = self.model.predict([inputs])[0]
        keys_to_press = []
        print(result)
        for r in result:
            keys_to_press.append(r > self.threshold)
        return keys_to_press

    def batch_learn(self):
        training_x, training_y = self.get_dataset()
        self.model.fit(training_x, training_y, epochs=10)
        self.model.save_weights(".\\controllers\\models\\ffnn.ckpt")