from pygame.key import get_pressed
from pygame.locals import K_LEFT, K_RIGHT, K_UP
from controllers.base_controller import BaseController


class Player(BaseController):
    def __init__(self):
        super().__init__()

    def get_keys_to_press(self, platforms, current_timer, player_pos):
        pressed = get_pressed()
        return [pressed[K_LEFT], pressed[K_RIGHT], pressed[K_UP]]
