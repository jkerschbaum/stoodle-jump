import pygame
import globals
from imblearn.over_sampling import SMOTE
from os import getcwd
from collections import Counter

vec = pygame.math.Vector2


class BaseController(pygame.sprite.Sprite):

    def __init__(self):
        super().__init__()
        self.supports_batch_learning = False
        self.supports_online_learning = False
        self.surf = pygame.Surface((30, 30))
        self.surf.fill((128, 255, 40))
        self.rect = self.surf.get_rect()
        self.pos = vec((10, 385))
        self.vel = vec(0, 0)
        self.acc = vec(0, 0)
        self.jumping = False
        self.last_pressed_jump = False
        self.score = 0
        self.last_presses = [False, False, False]

    def get_keys_to_press(self, platforms, current_timer, player_pos):
        """
        Provides inputs to make for current frame. Each controller implements own policy.
        :param platforms: Spritegroup of currently extant platforms
        :param current_timer: Remaining time
        :param player_pos: x, y position of the player sprite
        :return: List of 3 bools representing wheter each of the keys should be pressed on this frame
        :raises: NotImplementedError if not implemented by controller
        """
        raise NotImplementedError

    def move(self, platforms, current_timer, player_pos):
        """
        Moves player sprite in accordance with controllers policy
        :param platforms: Spritegroup of currently extant platforms
        :param current_timer: Remaining time
        :param player_pos: x, y position of the player
        :return: None
        """
        self.acc = vec(0, 0.5)
        pressed_keys = self.get_keys_to_press(platforms, current_timer, player_pos)
        self.last_presses = pressed_keys
        if pressed_keys[globals.key_left]:
            self.acc.x -= globals.ACC
        if pressed_keys[globals.key_right]:
            self.acc.x += globals.ACC
        if self.last_pressed_jump:
            if not pressed_keys[globals.key_jump]:
                self.cancel_jump()
            else:
                self.jump(platforms)
        else:
            if pressed_keys[globals.key_jump]:
                self.jump(platforms)
        self.last_pressed_jump = pressed_keys[globals.key_jump]
        self.acc.x += self.vel.x * globals.FRIC
        self.vel += self.acc
        self.pos += self.vel + 0.5 * self.acc
        if self.pos.x > globals.WIDTH:
            self.pos.x = 0
        if self.pos.x < 0:
            self.pos.x = globals.WIDTH
        self.rect.midbottom = self.pos
        self.update(platforms)

    def jump(self, platforms):
        """
        Handles jumping behaviour
        :param platforms: Spritegroup of currently extant platforms
        :return: None
        """
        hits = pygame.sprite.spritecollide(self, platforms, False)
        # Check that player is currently colliding with a plattform
        if hits and not self.jumping:
            self.jumping = True
            self.vel.y -= 15

    def cancel_jump(self):
        """
        Implements behaviour to stop jumping when no longer inputting jump key
        :return: None
        """
        if self.jumping:
            if self.vel.y < -3:
                self.vel.y = -3

    def update(self, platforms):
        """
        Update player position with respect to platforms. (Ie. stop players falling through plattforms)
        :param platforms: Spritegroup of currently extant platforms
        :return: None
        """
        hits = pygame.sprite.spritecollide(self, platforms, False)
        if self.vel.y > 0 and hits:
            if self.pos.y < hits[0].rect.bottom:
                if hits[0].point > self.score:
                    self.score = hits[0].point
                self.pos.y = hits[0].rect.top + 1
                self.vel.y = 0
                self.jumping = False

    def list_from_repr(self, rep : str):
        """
        Transforms representation of list of 3 bools to list
        :param rep: Representation of target list
        :return: Target list
        """
        values = rep[1:-1].split(", ")
        ret = []
        for i in range(3):
            ret.append(int(values[i] == '1'))
        return ret

    def get_dataset(self):
        """
        Gets full dataset, formatted (training_x, training_y)
        :return: Dataset
        """
        with open(getcwd() + "\\examples.dat", "r", encoding="utf-8") as file:
            data = file.read()
            lines = data.split("\n")
            training_x = []
            training_y = []
            for i in range(len(lines))[:-1]:
                thirds = lines[i].split("#")  # 0 = Numbers, 1 = Truth values, 2 = Timer
                pairs = thirds[0].split('|')
                data_x = []
                for p in range(len(pairs))[:-1]:
                    pairs[p] = pairs[p].split("~")
                    data_x.append(float(pairs[p][0]))
                    data_x.append(float(pairs[p][1]))
                data_x.append(float(thirds[2]))
                training_x.append(data_x)
                data_y = []
                for t in thirds[1].split(" ")[1:-1]:
                    data_y.append(int(t == "True"))
                training_y.append(repr(data_y))
            smote = SMOTE()
            training_x, res_y = smote.fit_resample(training_x, training_y)
            training_y = []
            for i in range(len(training_x)):
                training_y.append(self.list_from_repr(res_y[i]))
            print(Counter(res_y).most_common())
            return training_x, training_y

    def online_example(self, example):
        """
        Functionality for single-example online learning, if controller implements this
        :param example: Single example to learn from
        :return: None
        :raises: NotImplementedError if not implemented by controller
        """
        raise NotImplementedError

    def batch_learn(self):
        """
        Functionality for batch learning, if controller implements this
        :return: None
        :raises: NotImplementedError if not implemented by controller
        """
        raise NotImplementedError

    def reinforcement_learning(self, episodes, display):
        """
        Functionality for reinforcement learning, if controller implements this
        :param episodes: Number of episodes to learn from
        :param display: Display
        :return: None
        :raises: NotImplementedError if not implemented by controller
        """
        raise NotImplementedError
