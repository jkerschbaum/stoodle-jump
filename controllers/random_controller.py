from controllers.base_controller import BaseController
from random import randint


class Random(BaseController):

    def __init__(self):
        super().__init__()
        self.state = [False, False, False]

    def get_keys_to_press(self, platforms, current_timer, player_pos):
        for i in range(3):
            if randint(0, 4) == 0:
                self.state[i] = not self.state[i]
        return self.state
