from controllers.base_controller import BaseController
import tensorflow as tf
from tensorflow import keras
import numpy as np
from game import get_current_state, get_player, game

class DQN(BaseController):

    losses = []

    def __init__(self):
        super().__init__()
        self.TrainNet = DQN_Model(15, 8, [200, 200], 0.99, 1000, 10, 32, 1e-2)
        self.TargetNet = DQN_Model(15, 8, [200, 200], 0.99, 1000, 10, 32, 1e-2)
        self.epsilon = 0.5
        self.prev_observation = [0] * 15
        self.reward = 0
        self.iter = 0
        self.copy_step = 25

    def constructor_dummy(self):
        """
        Dummy that pretends to be a constructor, so that we can pass trained model rather than re-initialising
        :return: self
        """
        return self

    def get_keys_to_press(self, platforms, current_timer, player_pos):
        observation = get_current_state()
        action = self.TrainNet.get_action(observation, self.epsilon)
        self.prev_observation = observation
        self.reward += self.get_reward()
        exp = {'s': self.prev_observation, 'a': action, 'r': self.get_reward(), 's2': observation, 'done': False}
        self.TrainNet.add_experience(exp)
        loss = self.TrainNet.train(self.TargetNet)
        if isinstance(loss, int):
            self.losses.append(loss)
        else:
            self.losses.append(loss.numpy())
        self.iter += 1
        if self.iter % self.copy_step == 0:
            self.TargetNet.copy_weights(self.TrainNet)
        return self.get_keys_from_action(action)


    def reinforcement_learning(self, episodes, display):
        for i in range(episodes):
            game(False, True, DQN, display)
            print("Episode: ", i, "\nLoss: ", np.mean(self.losses))
            print("-------------------------------")

    def get_reward(self):
        """
        Function returns reward for latest action
        :return: Reward
        """
        return get_player().score

    def get_keys_from_action(self, action):
        """
        Turns predicted action into input combo
        :param action: Action to take
        :return: List of inputs
        """
        binary = "{0:b}".format(action)
        while len(binary) < 3:
            binary = '0' + binary
        keys = []
        for b in binary:
            keys.append(b == '1')
        return keys


class DQN_Model:

    experience = {'s': [], 'a': [], 'r': [], 's2': [], 'done': []}

    def __init__(self, num_states, num_actions, hidden_units, gamma, max_experiences, min_experiences, batch_size, lr):
        self.num_actions = num_actions
        self.batch_size = batch_size
        self.optimizer = tf.optimizers.Adam(lr)
        self.gamma = gamma
        self.model = InnerModel(num_states, hidden_units, num_actions)
        self.max_experiences = max_experiences
        self.min_experiences = min_experiences

    def predict(self, inputs):
        """
        Predict which action to take next, based on current observation
        :param inputs: Current observation
        :return: Action to take
        """
        for input in inputs:
            for i in range(len(input)):
                input[i] = np.array(input[i]).astype('float32')
        inputs = np.atleast_2d(inputs)
        for i in range(len(inputs)):
            inputs[i][1] = [inputs[i][1]]
        inputs = tf.ragged.constant(inputs)
        return self.model(inputs)

    def train(self, TargetNet):
        """
        Train TargetNet on current experiences.
        :param TargetNet: InnerModel to train
        :return: Loss
        """
        if len(self.experience['s']) < self.min_experiences:
            return 0
        ids = np.random.randint(low=0, high=len(self.experience['s']), size=self.batch_size)
        states = np.asarray([self.experience['s'][i] for i in ids], dtype=object)
        actions = np.asarray([self.experience['a'][i] for i in ids])
        rewards = np.asarray([self.experience['r'][i] for i in ids])
        states_next = np.asarray([self.experience['s2'][i] for i in ids], dtype=object)
        dones = np.asarray([self.experience['done'][i] for i in ids])
        value_next = np.max(TargetNet.predict(states_next), axis=1)
        actual_values = np.where(dones, rewards, rewards + self.gamma * value_next)

        with tf.GradientTape() as tape:
            selected_action_values = tf.math.reduce_sum(
                self.predict(states) * tf.one_hot(actions, self.num_actions), axis=1)
            loss = tf.math.reduce_mean(tf.square(actual_values - selected_action_values))
        variables = self.model.trainable_variables
        gradients = tape.gradient(loss, variables)
        self.optimizer.apply_gradients(zip(gradients, variables))
        return loss

    def get_action(self, states, epsilon):
        """
        Gets next action to perform, either randomly or from inner model
        :param states: Current Observation to predict from
        :param epsilon: Parameter representing the chance to use a random action
        :return: Action to take
        """
        if np.random.random() < epsilon:
            return np.random.choice(self.num_actions)
        else:
            return np.argmax(self.predict(np.atleast_2d(states))[0])

    def add_experience(self, exp):
        """
        Adds new experience to dictionary
        :param exp: experience to add
        :return: None
        """
        if len(self.experience['s']) >= self.max_experiences:
            for key in self.experience.keys():
                self.experience[key].pop(0)
        for key, value in exp.items():
            self.experience[key].append(value)

    def copy_weights(self, TrainNet):
        """
        Copy weights from other net
        :param TrainNet: Net to copy from
        :return: None
        """
        variables1 = self.model.trainable_variables
        variables2 = TrainNet.model.trainable_variables
        for v1, v2 in zip(variables1, variables2):
            v1.assign(v2.numpy())


class InnerModel(keras.Model):

    def __init__(self, num_states, hidden_units, num_actions):
        super(InnerModel, self).__init__()
        self.input_layer = keras.layers.InputLayer(input_shape=(num_states,))
        self.hidden_layers = []
        for i in hidden_units:
            self.hidden_layers.append(keras.layers.Dense(
                i, activation='tanh', kernel_initializer='RandomNormal'
            ))
        self.output_layer = keras.layers.Dense(num_actions, activation='linear', kernel_initializer='RandomNormal')

    #@tf.function
    def call(self, inputs):
        """
        Computes Model call
        :param inputs: Inputs for first model layer
        :return: Output of last model layer
        """
        #inputs = tf.constant(inputs)
        # Yes this is a very dirty fix, but that's basically all this is at this point
        l = inputs.to_list()
        l = [val for sublist in l for val in sublist]
        l = [val for sublist in l for val in sublist]
        l = [val for sublist in l for val in sublist]
        l = [l]
        inputs = tf.constant(l)
        z = self.input_layer(inputs)
        for layer in self.hidden_layers:
            z = layer(z)
        output = self.output_layer(z)
        return output
