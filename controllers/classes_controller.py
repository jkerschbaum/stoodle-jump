from controllers.base_controller import BaseController
from globals import plot_custom_model, WIDTH

import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers
import numpy as np


class Classes(BaseController):

    def __init__(self):
        super().__init__()
        self.supports_batch_learning = True
        self.threshold = 0.25
        self.model = self.create_model()
        try:
            # Load model
            self.model.load_weights(".\\controllers\\models\\functional3.ckpt")
        except:
            # Build new model
            self.batch_learn()

    def keys_from_action(self, action):
        binary = "{0:b}".format(action.numpy())
        while len(binary) < 3:
            binary = '0' + binary
        keys = []
        for b in binary:
            keys.append(b == '1')
        return keys

    def action_from_keys(self, keys):
        action = 0
        for i in range(len(keys)):
            action += (int(keys[i]) * (2 ** i))
        return action

    def create_model(self):
        """
        Create model for this Controller
        :return: Model
        """
        distances_input = keras.Input(shape=(14,), name="Distances Input")
        timer_input = keras.Input(shape=(1,), name="Timer Input")
        concat_inner = layers.concatenate([distances_input, timer_input])
        dense_5_0 = layers.Dense(10, activation='swish', name="Dense_Input_1", kernel_regularizer='l2')(concat_inner)
        dense_5 = layers.Dense(7, activation='swish', name="Dense_Input_2", kernel_regularizer='l2')(dense_5_0)
        dense_final = layers.Dense(20, activation='swish', name="Dense_Final")(dense_5)
        output_layer = layers.Dense(8, activation='softmax', name='Output')(dense_final)
        model = keras.Model(
            inputs=[distances_input, timer_input],
            outputs=[output_layer]
        )
        plot_custom_model(model, "functional3")
        model.compile(
            optimizer=keras.optimizers.RMSprop(1e-3),
            loss={
                "Output": keras.losses.BinaryCrossentropy(from_logits=False)
            },
            loss_weights={"Output": 1}
        )
        model.summary()
        return model


    def get_keys_to_press(self, platforms, current_timer, player_pos):
        positions = []
        for sprite in platforms.sprites():
            positions.append(sprite.rect.center)
        player_pos = (player_pos.x, player_pos.y)
        for i in range(len(positions)):
            positions[i] = [positions[i][0] - player_pos[0], positions[i][1] - player_pos[1]]
        # Normalising distances for screen wrap
        for pos in positions:
            if pos[0] > (WIDTH / 2):
                pos[0] = pos[0] - WIDTH
            elif pos[0] < (0 - (WIDTH / 2)):
                pos[0] = pos[0] + WIDTH
        positions = [val for sublist in positions for val in sublist]
        result = self.model(
            {
                'Distances Input': np.array([positions]),
                'Timer Input': np.array([current_timer])
            }
        )
        result = result[0]
        action = tf.argmax(result, 0)
        keys_to_press = self.keys_from_action(action)
        print(np.array(result).shape)
        print(tf.argmax(result))
        print(keys_to_press)
        print("------------")
        return keys_to_press

    def batch_learn(self):
        training_x, training_y = self.get_dataset()
        distances_data = []
        timer_data = []
        left_targets = []
        right_targets = []
        jump_targets = []
        actions = []
        for x in training_x:
            distances_data.append(x[:-1])
            timer_data.append([x[-1]])
        for y in training_y:
            left_targets.append([y[0]])
            right_targets.append([y[1]])
            jump_targets.append([y[2]])
        for i in range(len(training_y)):
            actions.append(self.action_from_keys([left_targets[i][0], right_targets[i][0], jump_targets[i][0]]))
        actions_targets = []
        for i in range(len(actions)):
            target = [0] * 8
            target[actions[i]] = 1
            actions_targets.append(target)
        distances_data = np.array(distances_data)
        timer_data = np.array(timer_data)
        actions_targets = np.array(actions_targets)
        self.model.fit(
            {"Distances Input": distances_data, "Timer Input": timer_data},
            {"Output": actions_targets},
            epochs=10
        )
        self.model.save_weights(".\\controllers\\models\\functional3.ckpt")