from controllers.base_controller import BaseController
from globals import plot_custom_model, WIDTH

import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers
import numpy as np


class Functional(BaseController):

    def __init__(self):
        super().__init__()
        self.supports_batch_learning = True
        self.threshold = 0.25
        self.model = self.create_model()
        try:
            # Load model
            self.model.load_weights(".\\controllers\\models\\functional.ckpt")
        except:
            # Build new model
            self.batch_learn()

    def create_model(self):
        """
        Create model for this Controller
        :return: Model
        """
        distances_input = keras.Input(shape=(14,), name="Distances Input")
        timer_input = keras.Input(shape=(1,), name="Timer Input")
        concat_inner = layers.concatenate([distances_input, timer_input])
        dense_5_0 = layers.Dense(10, activation='swish', name="Dense_Input_1", kernel_regularizer='l2')(concat_inner)
        dense_5 = layers.Dense(7, activation='swish', name="Dense_Input_2", kernel_regularizer='l2')(dense_5_0)
        output_left = layers.Dense(1, activation='sigmoid', name="Out_Left")(dense_5)
        output_right = layers.Dense(1, activation='sigmoid', name="Out_Right")(dense_5)
        distance_dense = layers.Dense(14, activation='softsign', name="Distance_Comp", kernel_regularizer='l2')(distances_input)
        concat_outer = layers.concatenate([distance_dense, output_left, output_right], name="Concat_Outer")
        dense_jump = layers.Dense(10, activation='swish', name="Dense_Jump", kernel_regularizer='l2')(concat_outer)
        output_jump = layers.Dense(1, activation='sigmoid', name="Out_Jump")(dense_jump)
        model = keras.Model(
            inputs=[distances_input, timer_input],
            outputs=[output_left, output_right, output_jump]
        )
        plot_custom_model(model, "functional")
        model.compile(
            optimizer=keras.optimizers.RMSprop(1e-3),
            loss={
                "Out_Left": keras.losses.BinaryCrossentropy(from_logits=False),
                "Out_Right": keras.losses.BinaryCrossentropy(from_logits=False),
                "Out_Jump": keras.losses.BinaryCrossentropy(from_logits=False)
            },
            loss_weights={"Out_Left": 0.25, "Out_Right": 0.25, "Out_Jump": 0.5},
        )
        model.summary()
        return model


    def get_keys_to_press(self, platforms, current_timer, player_pos):
        positions = []
        for sprite in platforms.sprites():
            positions.append(sprite.rect.center)
        player_pos = (player_pos.x, player_pos.y)
        for i in range(len(positions)):
            positions[i] = [positions[i][0] - player_pos[0], positions[i][1] - player_pos[1]]
        # Normalising distances for screen wrap
        for pos in positions:
            if pos[0] > (WIDTH / 2):
                pos[0] = pos[0] - WIDTH
            elif pos[0] < (0 - (WIDTH / 2)):
                pos[0] = pos[0] + WIDTH
        positions = [val for sublist in positions for val in sublist]
        result = self.model(
            {
                'Distances Input': np.array([positions]),
                'Timer Input': np.array([current_timer])
            }
        )
        keys_to_press = []
        print(np.array(result).shape)
        print("L: ", result[0][0][0].numpy(), result[0][0][0].numpy() > self.threshold)
        print("R: ", result[1][0][0].numpy(), result[1][0][0].numpy() > self.threshold)
        print("J: ", result[2][0][0].numpy(), result[2][0][0].numpy() > self.threshold)
        print("------------")
        for r in result:
            keys_to_press.append(r[0][0].numpy() > self.threshold)
        return keys_to_press

    def batch_learn(self):
        training_x, training_y = self.get_dataset()
        distances_data = []
        timer_data = []
        left_targets = []
        right_targets = []
        jump_targets = []
        for x in training_x:
            distances_data.append(x[:-1])
            timer_data.append([x[-1]])
        for y in training_y:
            left_targets.append([y[0]])
            right_targets.append([y[1]])
            jump_targets.append([y[2]])
        distances_data = np.array(distances_data)
        timer_data = np.array(timer_data)
        left_targets = np.array(left_targets)
        right_targets = np.array(right_targets)
        jump_targets = np.array(right_targets)
        self.model.fit(
            {"Distances Input": distances_data, "Timer Input": timer_data},
            {"Out_Left": left_targets, "Out_Right": right_targets, "Out_Jump": jump_targets},
            epochs=10
        )
        self.model.save_weights(".\\controllers\\models\\functional.ckpt")

