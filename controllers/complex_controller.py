from controllers.base_controller import BaseController
import numpy as np
from tensorflow import keras
from tensorflow.keras import layers
from globals import plot_custom_model, WIDTH

class Complex(BaseController):

    def __init__(self):
        super().__init__()
        self.supports_batch_learning = True
        self.threshold = 0.25
        self.model = self.create_model()
        try:
            # Load model
            self.model.load_weights(".\\controllers\\models\\complex.ckpt")
        except:
            # Build new model
            self.batch_learn()

    def create_model(self):
        """
        Create model for this Controller
        :return: Model
        """
        # TODO: Experiment
        plat_1_input = keras.Input(shape=(2,), name="Plat_1_Input")
        plat_2_input = keras.Input(shape=(2,), name="Plat_2_Input")
        plat_3_input = keras.Input(shape=(2,), name="Plat_3_Input")
        plat_4_input = keras.Input(shape=(2,), name="Plat_4_Input")
        plat_5_input = keras.Input(shape=(2,), name="Plat_5_Input")
        plat_6_input = keras.Input(shape=(2,), name="Plat_6_Input")
        plat_7_input = keras.Input(shape=(2,), name="Plat_7_Input")
        plat_1_dense = layers.Dense(2, activation='swish', name="Plat_1_Dense")(plat_1_input)
        plat_2_dense = layers.Dense(2, activation='swish', name="Plat_2_Dense")(plat_2_input)
        plat_3_dense = layers.Dense(2, activation='swish', name="Plat_3_Dense")(plat_3_input)
        plat_4_dense = layers.Dense(2, activation='swish', name="Plat_4_Dense")(plat_4_input)
        plat_5_dense = layers.Dense(2, activation='swish', name="Plat_5_Dense")(plat_5_input)
        plat_6_dense = layers.Dense(2, activation='swish', name="Plat_6_Dense")(plat_6_input)
        plat_7_dense = layers.Dense(2, activation='swish', name="Plat_7_Dense")(plat_7_input)
        concat_plat = layers.concatenate([plat_1_dense, plat_2_dense, plat_3_dense, plat_4_dense,
                                          plat_5_dense, plat_6_dense, plat_7_dense])
        platforms_dense = layers.Dense(10, activation='swish', name="Plat_Dense")(concat_plat)
        timer_input = keras.Input(shape=(1,), name="Timer_Input")
        concat_inner = layers.concatenate([platforms_dense, timer_input])
        dense_5_0 = layers.Dense(10, activation='swish', name="Dense_Input_1", kernel_regularizer='l2')(concat_inner)
        dense_5 = layers.Dense(7, activation='swish', name="Dense_Input_2", kernel_regularizer='l2')(dense_5_0)
        output_left = layers.Dense(1, activation='sigmoid', name="Out_Left")(dense_5)
        output_right = layers.Dense(1, activation='sigmoid', name="Out_Right")(dense_5)
        distance_dense = layers.Dense(14, activation='softsign', name="Distance_Comp", kernel_regularizer='l2')(concat_plat)
        concat_outer = layers.concatenate([distance_dense, output_left, output_right], name="Concat_Outer")
        dense_jump = layers.Dense(10, activation='swish', name="Dense_Jump", kernel_regularizer='l2')(concat_outer)
        output_jump = layers.Dense(1, activation='sigmoid', name="Out_Jump")(dense_jump)
        model = keras.Model(
            inputs=[plat_1_input, plat_2_input, plat_3_input, plat_4_input,
                    plat_5_input, plat_6_input, plat_7_input, timer_input],
            outputs=[output_left, output_right, output_jump]
        )
        plot_custom_model(model, "functional2")
        model.compile(
            optimizer=keras.optimizers.RMSprop(1e-3),
            loss={
                "Out_Left": keras.losses.BinaryCrossentropy(from_logits=False),
                "Out_Right": keras.losses.BinaryCrossentropy(from_logits=False),
                "Out_Jump": keras.losses.BinaryCrossentropy(from_logits=False)
            },
            loss_weights={"Out_Left": 0.25, "Out_Right": 0.25, "Out_Jump": 0.5},
        )
        model.summary()
        return model

    def get_keys_to_press(self, platforms, current_timer, player_pos):
        positions = []
        for sprite in platforms.sprites():
            positions.append(sprite.rect.center)
        player_pos = (player_pos.x, player_pos.y)
        for i in range(len(positions)):
            positions[i] = [positions[i][0] - player_pos[0], positions[i][1] - player_pos[1]]
        # Normalising distances for screen wrap
        for pos in positions:
            if pos[0] > (WIDTH / 2):
                pos[0] = pos[0] - WIDTH
            elif pos[0] < (0 - (WIDTH / 2)):
                pos[0] = pos[0] + WIDTH
        #positions = [val for sublist in positions for val in sublist]
        result = self.model(
            {
                "Plat_1_Input": np.array([positions[0]]),
                "Plat_2_Input": np.array([positions[1]]),
                "Plat_3_Input": np.array([positions[2]]),
                "Plat_4_Input": np.array([positions[3]]),
                "Plat_5_Input": np.array([positions[4]]),
                "Plat_6_Input": np.array([positions[5]]),
                "Plat_7_Input": np.array([positions[6]]),
                'Timer_Input': np.array([current_timer])
            }
        )
        keys_to_press = []
        print(np.array(result).shape)
        print("L: ", result[0][0][0].numpy(), result[0][0][0].numpy() > self.threshold)
        print("R: ", result[1][0][0].numpy(), result[1][0][0].numpy() > self.threshold)
        print("J: ", result[2][0][0].numpy(), result[2][0][0].numpy() > self.threshold)
        print("------------")
        for r in result:
            keys_to_press.append(r[0][0].numpy() > self.threshold)
        return keys_to_press

    def batch_learn(self):
        training_x, training_y = self.get_dataset()
        distances_data = []
        timer_data = []
        left_targets = []
        right_targets = []
        jump_targets = []
        for x in training_x:
            distances_data.append(x[:-1])
            timer_data.append([x[-1]])
        for y in training_y:
            left_targets.append([y[0]])
            right_targets.append([y[1]])
            jump_targets.append([y[2]])
        d0, d1, d2, d3, d4, d5, d6 = [], [], [], [], [], [], []
        # I may or may not have written this at literally 3 in the morning
        for x in distances_data:
            d0.append([])
            d0[-1].append(x[0])
            d0[-1].append(x[1])
            d1.append([])
            d1[-1].append(x[2])
            d1[-1].append(x[3])
            d2.append([])
            d2[-1].append(x[4])
            d2[-1].append(x[5])
            d3.append([])
            d3[-1].append(x[6])
            d3[-1].append(x[7])
            d4.append([])
            d4[-1].append(x[8])
            d4[-1].append(x[9])
            d5.append([])
            d5[-1].append(x[10])
            d5[-1].append(x[11])
            d6.append([])
            d6[-1].append(x[12])
            d6[-1].append(x[13])
        d0 = np.array(d0)
        d1 = np.array(d1)
        d2 = np.array(d2)
        d3 = np.array(d3)
        d4 = np.array(d4)
        d5 = np.array(d5)
        d6 = np.array(d6)
        timer_data = np.array(timer_data)
        left_targets = np.array(left_targets)
        right_targets = np.array(right_targets)
        jump_targets = np.array(right_targets)
        self.model.fit(
            {
                "Plat_1_Input": d0,
                "Plat_2_Input": d1,
                "Plat_3_Input": d2,
                "Plat_4_Input": d3,
                "Plat_5_Input": d4,
                "Plat_6_Input": d5,
                "Plat_7_Input": d6,
                'Timer_Input': timer_data
            },
            {"Out_Left": left_targets, "Out_Right": right_targets, "Out_Jump": jump_targets},
            epochs=10
        )
        self.model.save_weights(".\\controllers\\models\\complex.ckpt")