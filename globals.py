import pygame
from tensorflow.keras.utils import plot_model
import os
os.environ["PATH"] += os.pathsep + 'C:/Program Files (x86)/Graphviz/bin/'


def plot_custom_model(model, name):
    """
    Write image file plotting model to disk
    :param model: Model to plot
    :param name: Name for the File / Model / Controller
    :return: None
    """
    plot_model(model, ".\\resources\\" + name + ".png", show_shapes=True, show_dtype=True, show_layer_activations=True)

HEIGHT = 450
WIDTH = 400
ACC = 0.5
FRIC = -0.15
FPS = 60

key_left = 0
key_right = 1
key_jump = 2

all_sprites = pygame.sprite.Group()
platforms = pygame.sprite.Group()
